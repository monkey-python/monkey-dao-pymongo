Monkey DAO PyMongo
==================

Simple Data Access Object pattern implementation for [MongoDB](http://www.mongodb.org/) using [PyMongo API](https://api.mongodb.com/python/current/)

Installation guide
------------------

::

    pip install monkey-dao-pymongo

User guide
----------

   
